#pragma once 

namespace Math {

	class Addition {

	private:
		int a, b;

	public:
		int add(int a, int b);

	};
}
