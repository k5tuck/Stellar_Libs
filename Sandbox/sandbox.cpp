#include <iostream>
#include "stellarlibs/math/test.hpp"

using Math::Addition;

int main() {

    std::cout << "Welcome to StellarLibs" << std::endl;
    std::cout << Addition().add(101, 34);

    return 1;
}